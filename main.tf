terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "b" {
  bucket = "tyler-tf-test-bucket"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

resource "aws_s3_object" "object" {
  bucket = "${aws_s3_bucket.b.id}"
  key    = "g-hello-0.0.1-SNAPSHOT.jar"
  source = "build/libs/g-hello-0.0.1-SNAPSHOT.jar"
  acl = "public-read-write"
}

resource "aws_security_group" "allow_tls" {
  name        = "tyler_allow_tls"
  description = "Allow TLS inbound traffic"

   ingress {
    description      = "spring port from anywhere"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "ssh from anywhere"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_iam_role_policy" "test_policy" {
  name = "tyler_test_policy"
  role = aws_iam_role.test_role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ec2:Describe*",
          "s3:GetObject",
        ]
        Effect   = "Allow"
        Resource = "arn:aws:s3:::tyler-tf-test-bucket/*"
      },
    ]
  })
}

resource "aws_iam_role" "test_role" {
  name = "tyler_test_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_instance_profile" "tyler_test_profile2" {
  name = "tyler_test_profile"
  role = aws_iam_role.role.name
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "role" {
  name               = "tyler_test_role2"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_instance" "web" {
  ami = "ami-0172070f66a8ebe63"
  instance_type = "t3.micro"
  iam_instance_profile = "tyler_test_profile"
  key_name = "tyler-key-pair"
  security_groups = [aws_security_group.allow_tls.name]
  associate_public_ip_address = true
  tags = {
    Name = "tyler-g-hello"

  }

user_data = <<EOF
#! /bin/bash
sudo yum update -y
sudo yum install -y java-17-amazon-corretto-headless
aws s3api get-object --bucket "tyler-tf-test-bucket" --key "g-hello-0.0.1-SNAPSHOT.jar" "g-hello-0.0.1-SNAPSHOT.jar"
java -jar g-hello-0.0.1-SNAPSHOT.jar
EOF
}
